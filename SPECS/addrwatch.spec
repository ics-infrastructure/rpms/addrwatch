%global _hardened_build 1
%define _name addrwatch
%define version 1.0.2
%define url https://gitlab.esss.lu.se/ics-infrastructure/addrwatch
%define url2 https://github.com/fln/addrwatch

Name:		%{_name}
Version:	%{version}
Release:	0
Summary:	Monitoring IPv4/IPv6 and Ethernet address pairings
License:	GPLv3
URL:		%{url}
Source0:	%{url2}/releases/download/v1.0.2/addrwatch-1.0.2.tar.gz
Source1:	%{url}/-/raw/rawhide/addrwatch.service
Source2:	%{url}/-/raw/rawhide/addrwatch.sysconfig

%{?systemd_requires}
BuildRequires:	libpcap-devel, libevent-devel, systemd, mariadb-devel, sqlite-devel, gcc
BuildRequires:	autoconf automake
BuildRequires: make
Requires(pre):	shadow-utils


%description
It main purpose is to monitor network and log discovered Ethernet/IP pairings.

Main features of addrwatch:

 * IPv4 and IPv6 address monitoring
 * Monitoring multiple network interfaces with one daemon
 * Monitoring of VLAN tagged (802.1Q) packets.
 * Output to std-out, plain text file, syslog, sqlite3 db, MySQL db
 * IP address usage history preserving output/logging

Addrwatch is extremely useful in networks with IPv6 auto configuration (RFC4862)
enabled. It allows to track IPv6 addresses of hosts using IPv6 privacy
extensions (RFC4941).

%prep
%autosetup -p1

%build
autoreconf -fiv
%configure --enable-sqlite3 --enable-mysql LDFLAGS="-I/usr/include/mysql -L/usr/lib64/mysql"
%make_build

%install
%make_install
mkdir -p %{buildroot}%{_unitdir}/
install -p -m 644 %{SOURCE1} %{buildroot}%{_unitdir}/
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig/
install -p -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/addrwatch
mkdir -p %{buildroot}/var/lib/addrwatch

%files
%{_bindir}/addrwatch
%{_bindir}/addrwatch_stdout
%{_bindir}/addrwatch_mysql
%{_bindir}/addrwatch_syslog
%{_mandir}/man8/addrwatch.8*
%{_unitdir}/addrwatch.service
%config(noreplace) %{_sysconfdir}/sysconfig/addrwatch
%license COPYING
%attr(-, addrwatch, addrwatch) /var/lib/addrwatch

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d /var/lib/%{name} -s /sbin/nologin \
    -c "network neighborhoud watch" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%changelog
