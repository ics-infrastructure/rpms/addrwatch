# addrwatch

CentOS RPM for [addrwaatch](https://github.com/fln/addrwatch) addrwatch - a tool similar to arpwatch. It main purpose is to monitor network and log discovered ethernet/ip pairings.

Main features of addrwatch:

    IPv4 and IPv6 address monitoring
    Monitoring multiple network interfaces with one daemon
    Monitoring of VLAN tagged (802.1Q) packets.
    Output to stdout, plain text file, syslog, sqlite3 db, MySQL db
    IP address usage history preserving output/logging
